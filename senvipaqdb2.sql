CREATE DATABASE senvipaqdb;
\c senvipaqdb
CREATE TABLE usuarios(
	id SERIAL PRIMARY KEY,
	username TEXT UNIQUE,
	nombre VARCHAR(25),
	apellido VARCHAR(25),
	pass VARCHAR(32) NOT NULL,
	email TEXT,
	tel TEXT,
	rol INT NOT NULL
);
CREATE TABLE encargo(
	id SERIAL PRIMARY KEY,
	dinero FLOAT,
	descripcion TEXT
);
CREATE TABLE vehiculo(
	placas VARCHAR(10) PRIMARY KEY,
	tipo VARCHAR(20),
	seguro VARCHAR(8),
	ultima_revision TEXT,
	capacidad INT
);
CREATE TABLE paquete(
	id SERIAL PRIMARY KEY,
	tamanio INT,
	fragil BOOLEAN,
	descripcion TEXT
);
CREATE TABLE empleado(
	id SERIAL PRIMARY KEY,
	calle VARCHAR(20),
	colonia VARCHAR(20),
	municipio VARCHAR(20),
	num_telefono VARCHAR(10),
	nss VARCHAR(13),
	fecha_ingreso TEXT,
	nombre TEXT,
	apellidos TEXT,
	id_usuarios INT,
	status INT
);
CREATE TABLE vehiculo_empleado(
	id_vehiculo VARCHAR(10),
	id_empleado INT,
	disponible BOOLEAN
);
CREATE TABLE pedido(
	id SERIAL PRIMARY KEY,
	id_empleado INT NOT NULL,
	id_paquete INT,
	id_encargo INT,
	fecha DATE,
	ingreso FLOAT,
	emisor TEXT,
	receptor TEXT,
	fecha_limite DATE,
	tipo CHAR
);

ALTER TABLE pedido ADD CONSTRAINT fk_empleado_pedido FOREIGN KEY (id_empleado) REFERENCES empleado(id);
ALTER TABLE pedido ADD CONSTRAINT fk_paquete_pedido FOREIGN KEY (id_paquete) REFERENCES paquete(id);
ALTER TABLE vehiculo_empleado ADD CONSTRAINT fk_vehiculo_empleado FOREIGN KEY (id_vehiculo) REFERENCES vehiculo(placas);
ALTER TABLE vehiculo_empleado ADD CONSTRAINT fk_empleado_vehiculo FOREIGN KEY (id_empleado) REFERENCES empleado(id);
ALTER TABLE empleado ADD CONSTRAINT fk_empleado_usuarios FOREIGN KEY (id_usuarios) REFERENCES usuarios(id);



//pruebas hacia abajo :3
insert into usuarios(username,pass,email,tel,rol) values('moi','123','empleado@gmail.com','1234567',2);

/*PRUEBAS HACIA ABAJO :3*/
insert into usuarios(username,pass,email,tel,rol) values('empleado2','123','empleado@gmail.com','1234567',2);


insert into empleado(calle,colonia,municipio,num_telefono,nss,fecha_ingreso,nombre,apellidos,id_usuarios) 
	values ('ejido plan de ayala','info','valles','1234567890','12345671','20 nov','moi','san',6);

insert into empleado(calle,colonia,municipio,num_telefono,nss,fecha_ingreso,nombre,apellidos,id_usuarios) 
	values ('la 18','info','valles','1234567890','12345671','20 nov','Eduardo','Monter',4);

insert into empleado(calle,colonia,municipio,num_telefono,nss,fecha_ingreso,nombre,apellidos,id_usuarios) 

	values ('muyeres','info','valles','1234567890','12345671','20 nov','Esteba','Trejo',5);


select em.id,em.nombre,em.apellidos,veh.placas,veh.tipo,veh.capacidad from empleado as em join vehiculo_empleado as vem on em.id=vem.id_empleado join vehiculo as veh on vem.id_vehiculo=veh.placas where em.status=0;
select nombre from empleado where id=1;

	values ('muyeres','info','valles','1234567890','12345671','20 nov','Esteban','Trejo',5);

ALTER TABLE empleado ADD COLUMN status BOOLEAN NOT NULL DEFAULT 't';


/*MODIFICACION TABLA USUARIO*/
ALTER TABLE usuarios ADD COLUMN nombre VARCHAR(25);

ALTER TABLE usuarios ADD COLUMN apellido VARCHAR(25);

ALTER TABLE usuarios ADD COLUMN img TEXT;

/*MODIFICACION TABLA VEHICULO*/


