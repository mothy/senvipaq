<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/config.php';

$has_db = function ($app) {
  return function () use ($app) {
    if(is_null($app->db)){
      return $app->redirect($app->urlFor('500'),500);
    } else {
      return true;
    }
  };
};

$is_logged = function ($app) {
  return function () use ($app) {
    if(isset($app->session['user']) and !empty($app->session['user'])){
      return true;
    } else {
      return $app->redirect($app->urlFor('home'));
    }
  };
};

$app->get('/',$has_db($app), function() use($app) {
 if (isset($_SESSION['user'])) {
   return $app->redirect($app->urlFor('load-home'));
 }
  return $app->render("home.twig");
  //return $app->render('admin/admin-principal.twig');
})->name('home');


$app->get('/500/', function() use($app){
  $app->render('errors/500.twig');
})->name('500');


# Include Controllers here
include_once CONTROLLERS_DIR."usuarios/usuarios.php";
include_once CONTROLLERS_DIR."admin/admin.controllers.php";
include_once CONTROLLERS_DIR."user/user.controllers.php";

$app->run();
?>
