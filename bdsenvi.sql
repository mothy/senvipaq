
create table usuarios(
	id_user int primary key,
	rol int NOT NULL,
	username text NOT NULL UNIQUE,
	pass text NOT NULL,
	nombre varchar(60) NOT NULL,
	correo varchar(45),
	telefono varchar(10),
	calle varchar(15),
	colonia varchar(25),
	numero int,
	ciudad varchar(25),
	estado varchar(25),
	fechaContr varchar(15)
);

create table vehiculo(
	id_vehi serial primary key ,
	seguro varchar(15),
	tipo varchar(20),
	placas varchar(10),
	modelo varchar(10),
	fecha varchar(20)
);

create table paquete(
	idPaq serial primary key,
	tamano int,
	peso_aprox double precision,
	fragilidad int,
	status int
);

create table pedido(
	id_pedido serial primary key,
	id_cli int references usuarios(id_cli),
	id_chofer int references chofer(id_chofer),
	id_paquete int references paquete(idPaq),
	status int
);

create table seguro(
	ID_seg varchar(15) primary key,
	poliza varchar(15),
	tipo varchar(15),
	vigencia varchar(15)
);

CREATE TABLE chofer_vehiculo(
	id_asignar serial primary key,
	id_ int REFERENCES chofer(id_chofer),
	id_vehi int REFERENCES vehiculo(id_vehi),
	status int 
);


INSERT INTO cliente (id_user,nombre,ape_pat,ape_mat,correo,fecha_union) VALUES(currval('cliente_id_cli_seq'),'Administrador','Admin',' ejemplo','ejemplo','now');

insert into usuarios (username,pass,tel,rol) values ('admin','12345','4811231234',1);
insert into usuarios (rfc,username,pass,tel,rol) values ('1234567891234','chofer','12345','4811231234',2);
insert into usuarios (rfc,username,pass,tel,rol) values ('1234567891234','user','12345','4811231234',3);
insert into administrador (id_user,nombre, ape_pat, ape_mat,tel_cel,calle,colonia,municipio,correo) values (currval('administrador_id_admin_seq'),'Jesus Antonio','Amador','Soní','4811475172','Salinas','plan de ayala','Cd. Valles','soni@gmail.com');
insert into administrador (id_user,nombre, ape_pat, ape_mat,tel_cel,calle,colonia,municipio,correo) values (currval('administrador_id_admin_seq'),'Angel Esteban','Torres','Trejo','481123456','en una cajita','dentro de otra','Cd. Valles','esteban@gmail.com');
insert into administrador (id_user,nombre, ape_pat, ape_mat,tel_cel,calle,colonia,municipio,correo) values (currval('administrador_id_admin_seq'),'Eduardo','Monter','Alonso','481123456','en una cajita','dentro de otra','Cd. Valles','mothy@gmail.com');

///////////// Esto no :3
insert into paquete (idpaq,tamano,peso_aprox,fragilidad,status) values (1,3,1.25,2,0);
insert into paquete (idpaq,tamano,peso_aprox,fragilidad,status) values (2,2,3.05,2,1);
	insert into paquete (idpaq,tamano,peso_aprox,fragilidad,status) values (3,3,4.25,1,0);

insert into chofer (id_chofer,id_user,nombre,ape_pat,ape_mat,correo,calle,colonia,numero,ciudad,estado,nss,fechacontr) values (01,1,'juan','moctezuma','simon','correo@gmail.com','16 de sept','gavilan',51,'cd valles','slp','13215632','12-octubre-2017');

insert into pedido (id_pedido,id_cli,id_chofer,id_paquete) values (10,1,01,2);

INSERT INTO cliente (id_user,nombre,ape_pat,ape_mat,correo,fecha_union) VALUES(currval('usuarios_id_user_seq'),'asd','asd','asd','asd','now');

insert into usuarios (username,pass,tel,rol) values ('mothylag','12345','4811231234',1);
insert into usuarios (username,pass,tel,rol) values ('chofirete','12345','4811231234',2);
insert into usuarios (username,pass,tel,rol) values ('useringo','12345','4811231234',3);
insert into administrador (id_user,nombre, ape_pat, ape_mat,tel_cel,calle,colonia,municipio,correo) values (currval('usuarios_id_user_seq'),'Eduardo','Monter','Alonso','481123456','en una cajita','dentro de otra','Cd. Valles','mothy@gmail.com');
insert into administrador (id_user,nombre, ape_pat, ape_mat,tel_cel,calle,colonia,municipio,correo) values (currval('usuarios_id_user_seq'),'Jesus Antonio','Amador','Soní','4811475172','Salinas','plan de ayala','Cd. Valles','soni@gmail.com');
insert into administrador (id_user,nombre, ape_pat, ape_mat,tel_cel,calle,colonia,municipio,correo) values (currval('usuarios_id_user_seq'),'Angel Esteban','Torres','Trejo','481123456','en una cajita','dentro de otra','Cd. Valles','esteban@gmail.com');

ALTER TABLE usuarios ADD CONSTRAIN pass NO EMPTY;

