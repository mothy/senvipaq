<?php
$app->get("/profile-user/",function () use($app){
	
	$st = $app->db->prepare("SELECT id,nombre,apellido,email, tel FROM usuarios where id=1");
    $st->setFetchMode(PDO::FETCH_OBJ);
    $st->execute();
    $data['user'] = $st->fetchAll();
    
	$app->render("user/modals/profile-user.twig",$data);

})->name('load-profile-user');

$app->get("/send-package/",function () use($app){
	$app->render("user/modals/send-package.twig");
})->name('load-send-package');

$app->get("/send-history/",function () use($app){
	$app->render("user/modals/send-history.twig");
})->name('load-send-history');

$app->get("/package-tracking/",function () use($app){
	$app->render("user/modals/package-tracking.twig");
})->name('load-package-tracking');

$app->get("/billing/",function () use($app){
	$app->render("user/modals/billing.twig");
})->name('load-billing');

?>