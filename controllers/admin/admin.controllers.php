<?php

$app->get("/delete-user/",function () use($app){
	$st = $app->db->prepare("SELECT id,nombre,apellidos from empleado");
    $st->setFetchMode(PDO::FETCH_OBJ);
    $st->execute();
    $data['empleado'] = $st->fetchAll();

	$app->render("admin/modals/delete-user.twig",$data);
})->name('delete-user');

$app->get("/delete-empleado/",function () use($app){
	$app->render("admin/modals/delete-empleado.twig");
})->name('delete-empleado');

//carga de datos para aceptacion de empleados
$app->get("/validacion-empleados/",function () use($app){
	$st = $app->db->prepare("select em.id,em.nombre,em.apellidos,veh.placas,veh.tipo,veh.capacidad from empleado as em join vehiculo_empleado as vem on em.id=vem.id_empleado join vehiculo as veh on vem.id_vehiculo=veh.placas where em.status=0");
    $st->setFetchMode(PDO::FETCH_OBJ);
    $st->execute();
    $data['chofer'] = $st->fetchAll();
	$app->render("admin/modals/validacion-empleados.twig",$data);
})->name('load-validar');

//////////
$app->get("/aceptar-solicitud/(:id)",function($id_em=null) use($app){
	$st = $app->db->prepare("UPDATE empleado SET status=1 WHERE id=?");
 	$st->execute(array($id_em));
	$app->flash('mensaje', 'Aceptado con exito');
	$app->flashKeep();
	return $app->redirect($app->urlFor('load-user'));
})->name('aceptar-solicitud');


/////////
$app->get("/negar-solicitud/(:id)",function($id_em=null) use($app){	
	$st = $app->db->prepare("UPDATE empleado SET status=2 WHERE id=?");
 	$st->execute(array($id_em));
	return $app->redirect($app->urlFor('load-user'));
})->name('negar-solicitud');


//////////////
$app->get("/registrar-admin/",function () use($app){
	$app->render("admin/modals/registrar-admin.twig");
})->name('load-registrar-admin');

/*$app->post("/registro-vehiculo/",function () use ($app){

	$tipo=$_POST['tipo'];
	$placas=$_POST['placas'];
	$capacidad=$_POST['capacidad'];
	$seguro=$_POST['seguro'];
	$fecha=$_POST['fecha'];
	
	$st= $app->db->prepare("SELECT placas from vehiculo WHERE placas = ?");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute(array($placas));
	$data=$st->fetch();
	if (empty($data)) {
		$st = $app->db->prepare("INSERT INTO vehiculo(placas,tipo,seguro,ultima_revision,capacidad)  VALUES (?,?,?,?,?)");
		$st->execute(array($placas,$tipo,$seguro,$fecha,$capacidad));
		$app->render('admin/admin-principal.twig');	
	}else{
		$app->flash('mensaje', 'usuario y/o contraseña incorrectos');
		$app->flashKeep();
		$app->render('admin/admin-principal.twig');
	}
})->name('registro-vehi');*/



/*$app->post("/registro-chofer/",function () use ($app){	
		$nombre=$_POST['nombre'];
		$ape_pat=$_POST['ape_pat'];
		$ape_mat=$_POST['ape_mat'];
		$correo=$_POST['correo'];
		$calle=$_POST['calle'];
		$colonia=$_POST['colonia'];
		$numero=$_POST['numero'];
		$ciudad=$_POST['ciudad'];
		$estado=$_POST['estado'];
		$fecha= date('G:i  d-F-Y');
		$st = $app->db->prepare("INSERT INTO cliente (id_user,nombre,ape_pat,ape_mat,correo,calle,colonia,numero,ciudad,estado,fechacontr,ocupado) values(currval('usuarios_id_user_seq'),$nombre,$ape_pat,$ape_mat,$correo,$calle,$colonia,$numero,$ciudad,$estado,$fecha,0)");
		
		//$st->execute(array($nombre,$ape_pat,$ape_mat,$correo,$calle,$colonia,$numero,$ciudad,$estado,$fecha,0));
		$st->execute();
		$app->render('admin/admin-principal.twig');
})->name('registro-chofer');*/

 

 ?>
