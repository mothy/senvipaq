<?php
$app->get("/home/", function () use($app){
	$_SESSION = null;
	return $app->render("home.twig");
})->name('load-home');

$app->get("/registro/", function () use($app){
	$app->render('usuarios/registro.twig');
})->name('load-registro');

$app->get("/login/", function () use($app){
	$app->render('usuarios/login.twig');
})->name('load-login');

$app->get("/logout/",$is_logged, function () use($app){
	session_unset();
	session_destroy();
	return $app->redirect($app->urlFor('home'));
})->name('logout');

$app->post("/registro/", function () use($app){

	$user=$_POST['user'];
	$pass=$_POST['pass'];
	$email=$_POST['email'];
	$tel=$_POST['tel'];
	
	$st = $app->db->prepare("SELECT username FROM usuarios WHERE username = ? OR email = ?");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute(array($user, $email));
	$data = $st->fetch();
	if (empty($data)) {
		$st = $app->db->prepare("SELECT email from cliente where email= ?");
		$st->setFetchMode(PDO::FETCH_OBJ);
		$st->execute(array($email));
		$data = $st->fetch();
		if (empty($data)) {
		$st = $app->db->prepare("INSERT INTO usuarios (username,pass,email,tel,rol) VALUES(?,?,?,?,3)");
		$st->execute(array($user,$pass,$email,$tel));
		$app->render('usuarios/login.twig');
		}else{
		
		echo '<script language="javascript">alert("email ya esta registrado");</script>';
		}
	}else{
		echo '<script language="javascript">alert("usuario ya esta registrado");</script>';
		return $app->redirect($app->urlFor('load-registro'));
	}
})->name('registrar');



$app->get("/modal-integrantes/",function () use($app){
	$app->render("usuarios/modals/modal-integrantes.twig");
})->name('load-modal-integrantes');



$app->get("/modal-info/",function () use($app){
	$value = $_GET['val'];
	switch ($value) {
		case 'mision':
			$app->render('usuarios/modals/modal-mision.twig');
			break;
		case 'vision':
			$app->render('usuarios/modals/modal-vision.twig');
			break;
		case 'valores':
			$app->render('usuarios/modals/modal-valores.twig');
			break;
		default:
			break;
	}
})->name('load-modal-info');



$app->post("/login/",function() use($app){
	$user=$_POST['username'];
	$pass=$_POST['pass'];
	$st = $app->db->prepare("SELECT * from usuarios where username= ? and pass= ?");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute(array($user,$pass));
	$data['user'] = $st->fetch();
	$roles = array_column($data,'rol');
	if (!empty($data['user'])) {
		$_SESSION['rol'] = $roles[0];
		$_SESSION['user'] = $user;
		return $app->redirect($app->urlFor('load-user'));
	}else{
	$app->flash('mensaje', 'usuario y/o contraseña incorrectos');
	$app->flashKeep();
	return $app->redirect($app->urlFor('load-login'));
	}
})->name('login');


$app->get("/principal/",$is_logged($app), function () use($app){
	switch ($_SESSION['rol']) {
		case 1:
		    $app->render('admin/admin-principal.twig');
			break;
		case 3:
			$app->render('user/user-principal.twig');
		default:
			break;
	}
})->name('load-user');

?>
